using UnityEngine;

public class HealthManager : MonoBehaviour
{
    public float fullHealth;
    public float currentHealth;
    public bool playerDied = false;
    [SerializeField] Behaviour[] switchOnDeath;
    public GameManager gameManager;
    

    public void Awake()
    {
       
        currentHealth = fullHealth;

    }
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {
            AddDamage(20);
        }

    }
    public void AddDamage(float damage)
    {
        currentHealth -= damage;
        currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
       
        if (currentHealth <= 0)
        {
            playerDied = true;
            GetComponent<Rigidbody>().isKinematic = true;
            Destroy(gameObject);
            foreach (Behaviour b in switchOnDeath)
                b.enabled = !b.enabled;
            gameManager.gmCanvas();
        }
    }
}