using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    [SerializeField] Canvas gameOverCanvas;
    [SerializeField] Canvas victoryCanvas;
    private bool gameEnds = false;
    FPSController Player;
    public List<EnemyAi> enemies;
    public void Start()
    { 
        foreach (EnemyAi enemy in enemies)
            enemy.onDestroy.AddListener(() =>
            {
                RemoveEnemy(enemy);
            });
    }
    void RemoveEnemy(EnemyAi enemy)
    {
        enemies.Remove(enemy);
        
    }
    public void Playgame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void BackMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void gmCanvas()
    {
        if(gameEnds == false && Player == null)
        {
            gameEnds = true;
            gameOverCanvas.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
        }
    }
    public void vCanvas()
    {
        if(enemies.Count == 0)
        {
            victoryCanvas.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
        }
    }
}