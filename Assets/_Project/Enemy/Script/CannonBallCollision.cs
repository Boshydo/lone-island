using UnityEngine;

public class CannonBallCollision : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player"||collision.gameObject.tag == "Ground")
        {
            Destroy(gameObject);
        }
    }
}
