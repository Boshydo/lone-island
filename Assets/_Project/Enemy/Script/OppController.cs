using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OppController : MonoBehaviour
{
    private int opponentsLeft = 0;
    public Canvas vcanvas;
    FPSController player;
    public void opponentHasDied()
    {
        opponentsLeft--;
        print(opponentsLeft);

        if (opponentsLeft == 0)
        {
            player.enabled = false;
            vcanvas.enabled = true;

        }
    }
}
