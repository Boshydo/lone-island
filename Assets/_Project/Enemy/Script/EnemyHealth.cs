using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
   
    public float fullHealth;
    public float currentHealth;
    public bool EnemyDied = false;
 
   
    public void Awake()
    {
        currentHealth = fullHealth;
    }

    public void TakeDamage(float Hit)
    {
        currentHealth -= Hit;
        currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
        if (currentHealth <= 0)
        {
            EnemyDied = true;
            EnemyAi enemy = GetComponent<EnemyAi>();
            if (enemy)
            {
                enemy.onDestroy.Invoke();
                enemy.enabled = false;
            }
            Destroy(gameObject);
            
        }
    }
}
